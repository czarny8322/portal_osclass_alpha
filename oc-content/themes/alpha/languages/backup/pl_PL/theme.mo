��          �      |      �     �     �               !     *     1  	   7  =   A          �     �     �     �     �     �     �      �     �       9     �  W     V     Z     l     t  	   �     �     �     �  .   �     �     �               %     ,     8     ?  &   F     m     u  A   �                                      
          	                                                 404 Add listing Advertisement Browse categories Category Cities Color Configure Either something get wrong or the page doesn't exist anymore. Enable Theme Banners Location Name OOPS! Page Not Found! Regions Register Save Search Settings were successfully saved Take me home What are you looking for? When enabled, bellow banners will be shown in front page. Project-Id-Version: Alpha Osclass Theme
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-18 08:12+0100
PO-Revision-Date: 2019-07-20 19:27+0200
Last-Translator: Miso <miso546@azet.sk>
Language-Team: MB Themes <info@mb-themes.com>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_m
X-Poedit-Basepath: ../..
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.2.3
X-Poedit-SearchPath-0: languages/en_US
X-Poedit-SearchPath-1: .
 404 Dodaj ogłoszenie Reklama Kategorie ogłoszeń Kategoria Miasto Kolor Konfiguracja Coś poszło nie tak, lub strona nie istnieje. Włącz banery tematyczne Lokalizacja Nazwa OOPS! Strona nie znaleziona! Region Zarejestruj Zapisz Szukaj Ustawienia zostały zapisane poprawnie Powrót Czego poszukujesz? Jeśli włączysz, poniższe banery zostaną pokazane na stronie. 