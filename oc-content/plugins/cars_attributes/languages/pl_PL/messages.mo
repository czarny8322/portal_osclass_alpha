��    6      �  I   |      �     �     �     �     �     �  	   �     �     �     �     	          &     -  m   4     �     �     �     �     �     �     �     �     �     �     �     �                '     /     5     <     @  
   C     N     [     a     g     m     s     �     �     �     �     �     �     �     �               "     +     0  �  4  
   �     	     	     /	     @	     M	  	   U	     _	  	   l	     v	     �	     �	     �	  �   �	     H
     N
     U
     f
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                              (     F     K     X     a     i          �  %   �     �     �     �  
             "     1  	   7     A     E     &               #   +       
               -      ,   	                       !              *      %      1         '          2               6            0   "   4         3   )       $             (             /   5         .                              Add new Add new car type Add new make Add new model Auto Automatic Car details Car type Cars attributes Cars plugin Configure plugin Dealer Delete Deleting makes or models may end in errors. Some of those makes/models could be attached to some actual items Doors Edit Electric-hibrid Engine size (cc) Fuel Gasoline Gears Km Make Makes Manage makes Manage models Manage vehicle types Manual Mileage Model Models New No No results Num. Airbags Other Owner Power Seats Select a car type Select a make Select a model Select num. of airbags Select num. of doors Select num. of gears Select num. of seats Seller Transmission Vehicle types Warning Warranty Year Yes Project-Id-Version: OSClass 2.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-01 13:40+0100
PO-Revision-Date: 
Last-Translator: Juan Ramón <juanramon@osclass.org>
Language-Team: OSClass <info@osclass.org>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: ../..
X-Generator: Poedit 2.2.3
X-Poedit-SearchPath-0: .
 Dodaj nowy Dodaj nowy typ samochodu Dodaj nową markę Dodaj nowy model Automatyczna Automat Parametry Typ nadwozia Parametry Wtyczka Motoryzacja Konfiguracja wtyczki Firma Usuń Usuwanie marek lub modeli może zakończyć się błędami. Niektóre z tych marek / modeli mogą być dołączone do niektórych rzeczywistych przedmiotów Drzwi Edytuj Elektryk/Hybryda Pojemność silnika (cm³) Paliwo Benzyna Biegi KM Marka Marki Zarządzaj markami Zarządzaj modelami Zarządzaj typami samochodów Manualna Przebieg (km) Model Modele Nowy Nie Brak rezultatów Ilość poduszek powietrznych Inne Właściciel Moc (KM) Miejsca Wybierz typ samochodu Wybierz markę Wybierz model Wybierz ilość poduszek powietrznych Wybierz ilość dzwi Wybierz ilość biegów Wybierz ilość miejsc Sprzedawca Skrzynia biegów Typy pojazdów Uwaga Gwarancja Rok Tak 