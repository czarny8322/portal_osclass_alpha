v1.2.1

- Portability input radio issue has been fixed

v1.2.0

- Added checkbox "I agree" at create comment form

v1.0.1

- Fixed minor issues and improvements (thx @dev101, @cartagena68)

v1.0.0

- initial release